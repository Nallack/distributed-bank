# README #

##Summary##
This project consists of 4 main programs:

* Collector
* Analyser
* Director
* Bank


### Running ###
Each can be run from the respective class' "App" source file to get an interactive UI to make understanding, testing and debugging easier.
Atm i've made these simulate a terminal by overriding System.in and out.

alternatively there are other main methods that run in/out programs for each which are quick and easy to write and run.

### How to use this Repository ###
You should be able to simply download the source code by going to downloads then selecting the master branch.
Alternatively you should be able to use .git source code management tools using the HTTPS address available on the overview page.

I am using the Eclipse project files and eclipse .git plugin btw, but you can still use a simple text editor with the official .git utility program, and just use the src folder as your project folder.

If you want to post your changes here, I'd recommend you create a branch with your own name, keep pushing to your personal branch and then maybe merge them as a group every so often.

I'm not completely familiar with how the repo is meant to work but send me a message if you've got a problem. 

### Tutorials ###
If you find any good, relevant tutorials, post them here.

Cal 10th May:
The way I've been organizing the socket threads (by coincidence rather amazingly) is very similar to what this tutorial describes!

* http://www.oracle.com/technetwork/java/socket-140484.html

(may want to rename our "Session" classes to "Worker" instead like in the tutorial)

### Who do I talk to? ###

* Cal
* Other community or team contact




---------------------------------------------------------------------------------------------
Joe 7th May:
- Just uploading the notes that were made on Wednesday :)

![jpg010.jpg](https://bitbucket.org/repo/b989n5/images/163845429-jpg010.jpg)

![jpg011.jpg](https://bitbucket.org/repo/b989n5/images/2671014408-jpg011.jpg)

NB: May have to use port 2443 rather than 1443.
--