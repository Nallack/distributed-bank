package collector;

import java.io.*;
import java.security.PrivateKey;

import common.Coin;
import common.Cheque;
import common.EncryptionHelper;

public class CollectorMain {
	
	static String bankAddress = "localhost";
	static int bankPort = 1234;
	
	static String directorAddress = "localhost";
	static int directorPort = 4444;
		
	public static void main(String[] args) {
		
		final String security = "SSL";
		final String trustStore = "collector.ts";
		final char[] tsPassword = "collectorpassword".toCharArray();
		
		Collector collector = new Collector();
		collector.loadFactory(security, trustStore, tsPassword);
		System.out.println("Please enter 'demo' to start");
		commandLoop(collector);
	}
	
	private static void commandLoop(Collector collector) {
		BufferedReader input = new BufferedReader(
	   			   new InputStreamReader(
	   				   System.in));
		
		try {
			String command = null;
			while(true) {
				command = input.readLine();
				switch(command) {
				case "demo":
					System.out.println("Starting demo\nPlease enter bank IP Address");
					bankAddress = input.readLine();
					System.out.println("Please enter director IP Address");
					directorAddress = input.readLine();
					demo(collector);
					break;
				default:
					break;
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void demo(Collector collector) {
		
		System.out.println("Running Demo");
		
		System.out.println("Connecting to bank...");
		
		//connect to bank
		try { 
			collector.connectToBank(bankAddress, bankPort);
		} catch (IOException e) { 
			System.out.println("Error: couldnt connect to bank");
			System.out.println("Ending session..."); 
			System.out.println("Please enter 'demo' to start");
			return;
		}
		System.out.println("Connected");
		
		
		long collectorId;
		PrivateKey privatekey;
		
		//login, get private key, id and buy 10 coins
		try { 
			if(!collector.bankSession.login(collector.username, collector.password.toCharArray())) {
				collector.bankSession.createAccount(collector.username, collector.password.toCharArray());
				System.out.println("Account created");
				collector.bankSession.login(collector.username, collector.password.toCharArray());
			}
			
			System.out.println("Logged In");
			
			Coin[] coins = collector.bankSession.buy(10);
			System.out.println("got 10 coins");
			collectorId = collector.bankSession.getId();
			System.out.println("got id " + collectorId);
			privatekey = collector.bankSession.getKey();
			System.out.println("got key : " + EncryptionHelper.serializePrivateKey(privatekey));			
			collector.bankSession.disconnect();

			collector.wallet.add(coins);
			
		} catch (IOException e) { 
			System.out.println(e);
			return;
		}
		
		//subscribe to an analyser
		try {
			collector.connectToDirector(directorAddress, directorPort);
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Failed to contact director");
			System.out.println("Ending session..."); 
			System.out.println("Please enter 'demo' to start");
			return;
		}
		try {
			long analyserId = collector.directorSession.subscribe(collector.serviceName);
			
			if(analyserId == -1) {
				System.out.println("No such analyser exists");
				return;
			}
			System.out.println("Subscribed to " + collector.serviceName);
			
			//send temperatures to analyser and get result
			String temperatures = "2,3,4,2,4,7,4,2,6,8,4";
			
			Coin coin = collector.wallet.getCoin();
			Cheque cheque = new Cheque(collectorId, analyserId, coin);
			
			System.out.println("sending values: " + temperatures + " to " + collector.serviceName);
			String result = collector.directorSession.sendData(collector.serviceName, cheque, privatekey, temperatures);
			if(result != null) {
				collector.wallet.removeCoin(coin);
				System.out.println("result is " + result);
			} else {
				System.out.println("exchange failed");
			}
			
			collector.disconnectFromDirector();
			
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Failed get data");
		}
		
		
		System.out.println("Done!");
	}
}

