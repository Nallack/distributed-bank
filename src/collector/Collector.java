package collector;

import bank.BankClientSession;
import common.SocketHelper;
import common.Wallet;

import java.io.IOException;

import javax.net.ssl.*;

import director.DirectorClientSession;


public class Collector {
		
	/* Change these variables when using new collectors
	   'serviceName' is the analysis required:
	   Can be: average, median, mode, range    */ 
	
	String username = "collector1";
	String password = "password";
	String serviceName = "average";
	String walletFilename = "walletCollector.csv";
	
	
	Wallet wallet;
	
	SSLSocketFactory factory = null;
	BankClientSession bankSession = null;
	DirectorClientSession directorSession = null;
	
	public Collector() {

		wallet = new Wallet(walletFilename);
	}
	
	public void loadFactory(String security, String trustStore, char[] tsPassword)
	{
		try {
			factory = SocketHelper.createSSLSocketFactory(security, trustStore, tsPassword);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//region bank
	public void connectToBank(String address, int port) throws IOException {
		
		SSLSocket server = (SSLSocket)factory.createSocket(address, port);
		bankSession = new BankClientSession(server);		
	}
	
	public void disconnectFromBank() throws IOException {
		bankSession.disconnect();
		bankSession = null;
	}
	//endregion
	
	//region director
	public void connectToDirector(String address, int port) throws Exception {
		SSLSocket server = (SSLSocket)factory.createSocket(address, port);
		directorSession = new DirectorClientSession(server);
	}
	
	public void disconnectFromDirector() throws Exception {
		directorSession.disconnect();
		directorSession = null;
	}
	
	//endregion
}
