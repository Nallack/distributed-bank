package collector;

public enum CollectorType {
	 None,
	 Thermometer,
	 Odometer
}