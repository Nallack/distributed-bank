package common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Wallet {

	List<Coin> coins;
	File file;
	
	public Wallet(String filename) {
		this.coins = new ArrayList<Coin>();
		
		file = new File(filename);
		if (!file.exists()) {
			try {
				new FileWriter(file).close();
			}
			catch (Exception e) {
				System.out.println("couldnt create file " + filename);
			}
		}
		else if (!file.isDirectory()) {
			loadFromFile();
		}
	}
	
	public void add(Coin coin) {
		this.coins.add(coin);
		saveToFile();
	}
	
	public void add(Coin[] coins) {
		this.coins.addAll(Arrays.asList(coins));
		saveToFile();
	}
	
	public Coin getCoin() {
		return coins.get(0);
	}
	
	public boolean removeCoin(Coin coin) {
		if (coins.contains(coin)) {
			coins.remove(coin);
			saveToFile();
			return true;
		}
		return false;
	}
	
	private void loadFromFile() {
		BufferedReader br;
		try {
			br = new BufferedReader(
				 new FileReader(file));
			try {
				String line = null;
				while ((line = br.readLine()) != null) {
					coins.add(Coin.deserialize(line));
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			} finally {
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveToFile() {
		try {
			BufferedWriter bw = new BufferedWriter(
								new FileWriter(file));
			
			for (Coin coin : coins) {				
				bw.write(coin.serialize() + '\n');
			}
			bw.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
