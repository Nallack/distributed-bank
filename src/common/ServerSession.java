package common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;

import javax.net.ssl.SSLSocket;

public abstract class ServerSession {
	
	protected SSLSocket client;
	protected BufferedReader in;
	protected PrintStream out;
	
	
	public ServerSession(SSLSocket client) {
		this.client = client;
		
		try {
			in = new BufferedReader(
				 new InputStreamReader(
						 client.getInputStream()));
			
			out = new PrintStream(
					  client.getOutputStream());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
