package common;

import java.io.*;
import java.util.Arrays;
import javax.net.ssl.*;

/**
 * Session runnable that containts all operations for a client session
 * with the bank.
*/
public class InteractiveClientSession extends ClientSession implements Runnable {
	
	final BufferedReader input;
	
	Runnable interactiveIn = new Runnable() {
		public void run() {
			try {
				while(!server.isClosed()) {
					String message = in.readLine();
					System.out.println("server : " + message);
					
					if(message == null) {
						System.out.println("got null, Socket closed by server....");
						server.close();
						break;
					}
				}
			} catch(Exception e) {
				//e.printStackTrace();
			}
		}
	};
	
	Runnable interactiveOut = new Runnable() {
		public void run() {
			try {
				while(!server.isClosed()) {
					String clientMessage = input.readLine();
					if(clientMessage != null) {
						System.out.println("sending: " + clientMessage);	
						out.println(clientMessage);
					}
				}
			} catch(Exception e) {
				//e.printStackTrace();
			}
		}
	};
	
	public InteractiveClientSession(SSLSocket server) throws IOException {
		super(server);
		
		//this should allow you to send your input to a server
		input = new BufferedReader(
				new InputStreamReader(
					System.in));
	}

	public void run() {
		
		//clear the existing input
		try { while(input.ready()) { input.readLine(); } }
		catch (IOException e) {}
		
		Thread inThread = new Thread(interactiveIn);
		Thread outThread = new Thread(interactiveOut);
		
		try {
			System.out.println("Connected to Server");
			System.out.println("Address : " + server.getInetAddress());
			System.out.println("Protocols : " + Arrays.toString(server.getEnabledProtocols()));
			System.out.println("Cipher Suites : " + Arrays.toString(server.getEnabledCipherSuites()));
			
			System.out.println("Handshaking...");
			server.startHandshake();
			
			//isReady() doesnt work...
			//so just utilize the blocking call on two threads

			
			inThread.start();
			outThread.start();
			
			inThread.join();
			//while(!server.isClosed()) {
			//	Thread.sleep(1000);
			//}
		}
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			inThread.interrupt();
			outThread.interrupt();
		}
	}

}
