package common;

public class Coin {
	int id;
	String value;
	
	public Coin(int id, String value) {
		this.id = id;
		this.value = value;			
	}
	
	//region get/set
	public int getId() { return id; }
	public String getData(int id) { return value; }
	//endregion
	

	public String serialize() {
		return id + "," + value;
	}
	
	public static Coin deserialize(String data) {
		String[] split = data.split(",");
		return new Coin(Integer.parseInt(split[0]), split[1]);
	}
	
	public String toString() {
		return String.format("id: %d\tvalue: %s", id, value);
	}
	
	public boolean equals(Coin other) {
		return this.id == other.id && this.value.equals(other.value);
	}
}