package common;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

public class ActionEventRaiser {
	private List<ActionListener> listeners;
	
	public ActionEventRaiser() {
		listeners = new LinkedList<ActionListener>();
	}
	
	public void addActionListener(ActionListener l) {
		listeners.add(l);
	}
	
	public void raise(ActionEvent e) {
		for(ActionListener l : listeners) {
			l.actionPerformed(e);
		}
	}
}
