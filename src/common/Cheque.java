package common;

public class Cheque {
	public long fromAccount;
	public long toAccount;
	public Coin coin;
	
	public Cheque(long from, long to, Coin coin) {
		this.fromAccount = from;
		this.toAccount = to;
		this.coin = coin;
	}
	
	public String serialize() {
		return String.format("%d:%d:%s", fromAccount, toAccount, coin.serialize()); //.serialize() or not???
	}
	
	public static Cheque deserialize(String data) {
		String[] split = data.split(":");
		return new Cheque(Long.parseLong(split[0]),
						  Long.parseLong(split[1]),
						  Coin.deserialize(split[2]));
	}
}
