//tutorials from
//http://javamex.com/tutorials/cryptography/rsa_encryption.shtml

//RSA vs DSA and algorithm explanation
//http://stackoverflow.com/questions/2841094/what-is-the-difference-between-dsa-and-rsa

package common;

import java.math.BigInteger;
import java.security.*;
import java.security.spec.*;

import javax.crypto.Cipher;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;


public class EncryptionHelper {
	
	public static void main(String[] args) {
		String original = "HELLO!";
		String encrypted = null;
		String decrypted = null;
		
		KeyPair keyPair = rsaGenerateKey(1L);
		
		String pubk = serializePublicKey(keyPair.getPublic());
		System.out.println("1 : " + pubk);
		String privk = serializePrivateKey(keyPair.getPrivate());
		System.out.println("2 : " + privk);
		
		
		PublicKey pubkey = deserializePublicKey(pubk);
		PrivateKey privkey = deserializePrivateKey(privk);
		
		try {
			System.out.println(original);
			
			encrypted = rsaEncrypt(original, privkey);
			
			System.out.println(encrypted);
			
			decrypted = rsaDecrypt(encrypted, pubkey);
			
			System.out.println(decrypted);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static KeyPair rsaGenerateKey(long seed) {
		try {
			KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
			SecureRandom sr = new SecureRandom();
			sr.setSeed(seed);
			kpg.initialize(2048, sr);
			return kpg.genKeyPair();
		} catch(NoSuchAlgorithmException e) { 
			System.err.println(e); 
			return null;
		}
	}
	
	public static Key aesGenerateKey(byte[] input){
		return new SecretKeySpec(input, "AES");
	}
	
	public static String aesEncrypt(Key key, String data) throws Exception {
		Cipher c = Cipher.getInstance("AES");
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] eValue = c.doFinal(data.getBytes());
		return DatatypeConverter.printHexBinary(eValue);
		//return new BASE64Encoder().encode(eValue);
	}
	
	public static String aesDecrypt(Key key, String data) throws Exception {
		Cipher c = Cipher.getInstance("AES");
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] dValue = c.doFinal(DatatypeConverter.parseHexBinary(data));
		//byte[] dValue = c.doFinal(new BASE64Decoder().decodeBuffer(data));
		return new String(dValue);
	}
	
	public static String serializePrivateKey(PrivateKey key) {
		try {
			KeyFactory factory = KeyFactory.getInstance("RSA");
			RSAPrivateKeySpec spec = factory.getKeySpec(key, RSAPrivateKeySpec.class);
			String mod = spec.getModulus().toString(16);
			String exp = spec.getPrivateExponent().toString(16);
			return mod + ":" + exp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String serializePublicKey(PublicKey key) {
		try {
			KeyFactory factory = KeyFactory.getInstance("RSA");
			RSAPublicKeySpec spec = factory.getKeySpec(key, RSAPublicKeySpec.class);
			String mod = spec.getModulus().toString(16);
			String exp = spec.getPublicExponent().toString(16);
			return mod + ":" + exp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static PrivateKey deserializePrivateKey(String data) {
		try {
			String[] split = data.split(":");
			BigInteger mod = new BigInteger(split[0], 16);
			BigInteger exp = new BigInteger(split[1], 16);
			
			RSAPrivateKeySpec spec = new RSAPrivateKeySpec(mod, exp);
			KeyFactory factory = KeyFactory.getInstance("RSA");
			return factory.generatePrivate(spec);
		} catch (Exception e) {
			System.err.println(e);
		}
		return null;
	}
	
	public static PublicKey deserializePublicKey(String data) {
		try {
			String[] split = data.split(":");
			BigInteger mod = new BigInteger(split[0], 16);
			BigInteger exp = new BigInteger(split[1], 16);
			
			RSAPublicKeySpec spec = new RSAPublicKeySpec(mod, exp);
			KeyFactory factory = KeyFactory.getInstance("RSA");
			return factory.generatePublic(spec);
		} catch (Exception e) {
			System.err.println(e);
		}
		return null;
	}
	
	static public String rsaEncrypt(String data, Key key) throws Exception {
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] cipherData = cipher.doFinal(data.getBytes());
		return DatatypeConverter.printHexBinary(cipherData);
		//return new BASE64Encoder().encode(cipherData);
	}
	
	static public String rsaDecrypt(String data, Key key) throws Exception {
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] cipherData = cipher.doFinal(DatatypeConverter.parseHexBinary(data));
		//byte[] cipherData = cipher.doFinal(new BASE64Decoder().decodeBuffer(data));
		return new String(cipherData);
	}
}
