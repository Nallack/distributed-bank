package common;

import java.io.FileInputStream;
import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class SocketHelper {
	
	public static SSLServerSocketFactory createSSLServerSocketFactory(String type, String keyStore, char[] passphrase) throws Exception {
		SSLContext ctx = SSLContext.getInstance(type);
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		KeyStore ks = KeyStore.getInstance("JKS");
		ks.load(new FileInputStream(keyStore), passphrase);
		kmf.init(ks, passphrase);
		ctx.init(kmf.getKeyManagers(), null, null);
		return ctx.getServerSocketFactory();
	}

	public static SSLSocketFactory createSSLSocketFactory(String type, String trustStore, char[] passphrase) throws Exception {	
		SSLContext ctx = SSLContext.getInstance(type);
		TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
		KeyStore ts = KeyStore.getInstance("JKS");	
		ts.load(new FileInputStream(trustStore), passphrase);
		tmf.init(ts);
		ctx.init(null, tmf.getTrustManagers(), null);	
		return ctx.getSocketFactory();
	}
}
