package common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

import javax.net.ssl.SSLSocket;

public abstract class ClientSession {
	
	public SSLSocket server;
	protected BufferedReader in;
	protected PrintStream out;
	
	public ClientSession(SSLSocket server) throws IOException {
		this.server = server;
		
		try {
			server.startHandshake();
			
			in = new BufferedReader(
				 new InputStreamReader(
				 server.getInputStream()));
						
			out = new PrintStream(
				  server.getOutputStream());
		}
		catch(IOException e) {
			throw new IOException("Error: Failed to create i/o stream with server");
		}
	}
	
	
	public void disconnect() throws IOException {
		server.close();
	}
}
