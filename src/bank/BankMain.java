package bank;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BankMain {

	public static void main(String[] args) {
		
		String security = "SSL";
		String keyStore = "bank.jks";
		char[] ksPassword = "bankpassword".toCharArray();
		int serverPort = 1234;
		
		Bank bank = new Bank();
		bank.loadFactory(security, keyStore, ksPassword);
		bank.startServer(serverPort);
		
		System.out.println("Bank Server Ready on port " + serverPort);
	
		commandLoop(bank);
		
	}
	
	private static void commandLoop(Bank bank) {
		BufferedReader input = new BufferedReader(
				   new InputStreamReader(System.in));

		try {
			String command = null;
			while(true) {
				command = input.readLine();
				switch(command) {
				case "vault":
					System.out.println(bank.vault);
					break;
				case "accounts":
					System.out.println(bank.accounts);
					break;
				default:
					printHelp();
					break;
				}			
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static private void printHelp() {
		System.out.println(	  "commands:\n"
							+ "acccounts\n"
							+ "vault");
	}
}
