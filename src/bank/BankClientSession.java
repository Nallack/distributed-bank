package bank;

import java.io.*;
import java.security.PrivateKey;

import javax.net.ssl.*;

import common.ClientSession;
import common.Coin;
import common.EncryptionHelper;

/**
 * Session runnable that containts all operations for a client session
 * with the bank.
*/
public class BankClientSession extends ClientSession {
		
	public boolean loggedIn = false;
	
	public BankClientSession(SSLSocket server) throws IOException {
		super(server);
		String message = in.readLine();
		if(!message.equals("<BANK>")) {
			disconnect();
			throw new IOException("Not a bank");
		}
	}
	
	public void disconnect() throws IOException {
		loggedIn = false;
		server.close();
	}
	
	/**
	 * Attempt to create an account with given username and password.
	 * @param username
	 * @param password
	 * @return true if the account was created, false otherwise.
	 * @throws IOException if the connection abruptly is closed.
	 */
	public boolean createAccount(String username, char[] password) throws IOException {
		try {
			out.println("<CREATE>");
			out.println(username);
			out.println(password);
			
			if(in.readLine().equals("<SUCCESS>")) return true;
			else return false;
		
		} catch (IOException e) {
			disconnect();
			throw e;
		}
	}
	
	public boolean login(String username, char[] password) throws IOException {
		try {
			out.println("<LOGIN>");
			out.println(username);
			out.println(password);
			//TODO debug
			String message = in.readLine();
			if(message.equals("<SUCCESS>")) {
				loggedIn = true;
				return true;
			} else {
				loggedIn = false;
				return false;
			}
		} catch(IOException e) {
			disconnect();
			throw e;
		}
	}
	
	public long getId() throws IOException {
		out.println("<GETID>");
		String message = in.readLine();
		long accountId = Long.parseLong(message);
		return accountId;
	}
	
	public PrivateKey getKey() throws IOException {
		out.println("<GETKEY>");
		String message = in.readLine();
		return EncryptionHelper.deserializePrivateKey(message);
	}
	
	//must login first
	public Coin cashin(long fromAccountId, String encryptedCheque) throws IOException {
		//if(!loggedIn) return null;
		out.println("<CASHIN>");
		out.println(fromAccountId);
		out.println(encryptedCheque);
		if(!in.readLine().equals("<SUCCESS>")) return null;
		return Coin.deserialize(in.readLine());
		
	}
	
	public Coin[] buy(int amount) throws IOException {
		try {
			out.println("<BUY>");
			out.println(amount);
			
			String message = in.readLine();
			if(message.equals("<COINS>")) {
				int counter = 0;
				Coin[] coins = new Coin[amount];
				while(!(message = in.readLine()).equals("<END>")) {
					coins[counter++] = Coin.deserialize(message);
				}
				return coins;
			}
			else return null;
		} catch(IOException e) {
			disconnect();
			throw e;
		}
	}
	
	public void logout() throws IOException {		
		try {
			out.println("<LOGOUT>");
			String message = in.readLine();
			if(message.equals("<SUCCESS>")) loggedIn = false;
		} catch (IOException e) {
			disconnect();
			throw e;
		}		
	}
}