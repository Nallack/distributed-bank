package bank;

import java.security.KeyPair;


import common.EncryptionHelper;


public class Account {
	long id;
	String username;
	char[] password;
	KeyPair keyPair;
	
	public Account(long id, String username, char[] password, KeyPair keyPair) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.keyPair = keyPair;
	}
	
	@Override
	public boolean equals(Object o) {
		return false;
	}
	
	public String serialize() {
		//bank keeps the public, user keeps the private
		return id + ","
			 + username + "," 
			 + new String(password) + ","
			 + EncryptionHelper.serializePublicKey(keyPair.getPublic()) + ","
			 + EncryptionHelper.serializePrivateKey(keyPair.getPrivate());		
	}
	
	public static Account deserialize(String data) {
		String[] split = data.split(",");
		return new Account(Long.parseLong(split[0], 10),
				split[1],
				split[2].toCharArray(),
				new KeyPair(EncryptionHelper.deserializePublicKey(split[3]),
						EncryptionHelper.deserializePrivateKey(split[4])));
	}
	
	public String toString() {
		return String.format("id: %d\tname: %s", id, username);
	}
}