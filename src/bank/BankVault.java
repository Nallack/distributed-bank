package bank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import common.Cheque;
import common.Coin;
import common.EncryptionHelper;

public class BankVault {
	
	File file;
	
	List<CoinRegister> registers;
	Key secretKey;
	
	public BankVault(String filename, byte[] coinKey) {
		this.registers = new ArrayList<CoinRegister>();
		this.secretKey = EncryptionHelper.aesGenerateKey(coinKey);
		
		this.file = new File(filename);
		if (!file.exists()) {
			try {
				new FileWriter(file).close();
			}
			catch (Exception e) {
				System.out.println("couldnt create file " + filename);
			}
		}
		else if (!file.isDirectory()) {
			loadFromFile();
		}
	}
	
	public Coin buyCoin(long accountId) {
		for(CoinRegister register : registers) {
			if(register.accountId == -1) {
				register.accountId = accountId;
				saveToFile();
				return register.coin;
			}
		}
		
		//if no free coins make a new one
		try {
			String coinData = EncryptionHelper.aesEncrypt(secretKey, Integer.toString(registers.size()));
			CoinRegister register = new CoinRegister(accountId, new Coin(registers.size(), coinData));
			registers.add(register);
			saveToFile();
			return register.coin;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	//buy multiple coins at once
	public Coin[] buyCoins(int amount, long accountId) {
		
		int counter = 0;
		Coin[] coins = new Coin[amount];
		
		for(CoinRegister register : registers) {
			if(register.accountId == -1) {
				register.accountId = accountId;
				coins[counter++] = register.coin;
				if(counter == amount - 1) return coins;
			}
		}
		
		//no more free coins, make new ones
		while(counter < amount) {
			try {
				String coinData = EncryptionHelper.aesEncrypt(secretKey, Integer.toString(registers.size()));
				CoinRegister register = new CoinRegister(accountId, new Coin(registers.size(), coinData));
				registers.add(register);
				coins[counter++] = register.coin;
			} catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		saveToFile();
		return coins;
	}
	
	public void sellCoin(Coin[] coins) {
		
	}
	
	public Coin cashinCheque(Cheque cheque) {
		
		for(CoinRegister register : registers) {
			System.out.println(register.serialize());
			if(register.accountId == cheque.fromAccount) {
				if(register.coin.equals(cheque.coin)) {
					register.accountId = cheque.toAccount;
					saveToFile();
					return register.coin;
				}
			}
		}
		return null;
	}
	
	private void saveToFile() {
		try {
			BufferedWriter bw = new BufferedWriter(
								new FileWriter(file));
			
			for(CoinRegister register : registers) {
				bw.write(register.serialize() + '\n');
			}
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void loadFromFile() {
		try {
			BufferedReader br = new BufferedReader(
								new FileReader(file));
			
			String line = null;
			while((line = br.readLine()) != null) {
				CoinRegister register = CoinRegister.deserialize(line);
				registers.add(register);
			}
			br.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(CoinRegister register : registers) {
			sb.append(register.accountId + " : " + register.coin + '\n');
		}
		return sb.toString();
	}
}
