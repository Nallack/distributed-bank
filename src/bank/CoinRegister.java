package bank;

import common.Coin;

	public class CoinRegister {
		long accountId;
		Coin coin;
		
		public CoinRegister(long accountId, Coin coin) {
			this.accountId = accountId;
			this.coin = coin;
		}
		
		public String serialize() {
			return accountId + ":" + coin.serialize();
		}
		
		//deserializer
		public static CoinRegister deserialize(String data) {
			String[] split = data.split(":");
			return new CoinRegister(Long.parseLong(split[0]), Coin.deserialize(split[1]));
		}
	}