package bank;

import java.io.IOException;

import javax.net.ssl.*;

import common.SocketHelper;

// Bank contains a large list of coins
// and accounts with who has which coins in each
//
public class Bank {
	
	SSLServerSocketFactory factory;
	SSLServerSocket serverSocket;
	Thread serverThread;
	
	final byte[] coinKey = "asdasfafaaaaaaaf".getBytes();
	final String vaultFile = "vault.csv";
	BankVault vault;
	
	final String accountFile = "account.csv";
	AccountCollection accounts;
	
	int serverPort = 1234;
	
	public Bank() {
		accounts = new AccountCollection(accountFile);
		vault = new BankVault(vaultFile, coinKey);
	}
	
	public void loadFactory(String security, String keyStore, char[] ksPassword) {
		try {
			factory = SocketHelper.createSSLServerSocketFactory(security, keyStore, ksPassword);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void startServer(int serverPort) {
		this.serverPort = serverPort;
		serverThread = new Thread(dispatcher);
		serverThread.start();
	}
	
	public void stopServer() {
		serverThread.interrupt();
	}
	
	Runnable dispatcher = new Runnable() {
		public void run() {
			
			try {
				serverSocket = (SSLServerSocket)factory.createServerSocket(serverPort);
				try {
					while(true) {
						System.out.println("Waiting for client...");
						SSLSocket socket = (SSLSocket)serverSocket.accept();
						System.out.println("Client accepted");
						new Thread(new BankServerSession(Bank.this, socket)).start();
					}
				} catch(IOException e) {
					throw e;
				} finally {
					serverSocket.close();
				}
				
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	};
}
