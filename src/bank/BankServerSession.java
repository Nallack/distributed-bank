package bank;

import java.io.*;
import java.security.PublicKey;

import javax.net.ssl.*;

import common.Coin;
import common.Cheque;
import common.EncryptionHelper;
import common.ServerSession;

//
//
//


/**
 * Represents a single session the bank has with a client.
 * will need to support multithreaded access to bank and its registry.
 */
public class BankServerSession extends ServerSession implements Runnable {
	
	private Bank bank;
	
	public BankServerSession(Bank bank, SSLSocket client) {
		super(client);
		this.bank = bank;
	}
	
	public void run() {
		System.out.println("Session started: " + Thread.currentThread().getName());		
		try {
			//out.write("Hello\nBank is temporarily unavailable");
			//out.flush();
			//return;
			//socket.setSoTimeout(1000);
			//SSLSession session = socket.getSession();
						
			out.println("<BANK>");
			String command = null;
			String username = null;
			String password = null;
			Account account = null;
			outerloop:
			while((command = in.readLine()) != null) {
				System.out.println("client : " + command);
				connectloop:
				switch(command) {
				case "<CREATE>":
					username = in.readLine();
					password = in.readLine();
					
					System.out.println("Creating Account " + username + "...");
					account = bank.accounts.createAccount(username, password.toCharArray());
					if(account == null) {
						System.out.println("Account creation failed");
						out.println("<FAIL>");
					}
					else {
						System.out.println("Account creation successful");
						out.println("<SUCCESS>");
					}
					//dont login automatically
					account = null;
					break;
				case "<LOGIN>":
					username = in.readLine();
					password = in.readLine();
					
					account = bank.accounts.getAccountByLogin(username, password.toCharArray());
					if(account == null) {
						out.println("<FAIL>");
					} else {
						out.println("<SUCCESS>");
						//accepting logged in commands
						while((command = in.readLine()) != null) {
							System.out.println("client : " + command);
							switch(command) {
								case "<LOGOUT>":
									username = null;
									password = null;
									out.println("<SUCCESS>");
									break connectloop;
								case "<GETKEY>":
									out.println(EncryptionHelper.serializePrivateKey(account.keyPair.getPrivate()));
									break;
								case "<GETID>":
									out.println(account.id);
									break;
								case "<BUY>":
									int amount = Integer.parseInt(in.readLine());
									out.println("<COINS>");
									Coin[] coins = bank.vault.buyCoins(amount, account.id);
									for(int i = 0; i < coins.length; i++) {
										out.println(coins[i].serialize());
									}
									out.println("<END>");
									
									break;
								case "<SELL>":
									break;
								case "<CASHIN>":
									long fromAccountId = Long.parseLong(in.readLine());
									String encryptedCheque = in.readLine();
									
									try {
										Account fromAccount = bank.accounts.getAccountById(fromAccountId);
																				
										PublicKey publicKey = fromAccount.keyPair.getPublic();
		
										String decryptedCheque = EncryptionHelper.rsaDecrypt(encryptedCheque, publicKey);
										Cheque cheque = Cheque.deserialize(decryptedCheque);
										System.out.println("decrypted cheque: " + decryptedCheque);
										
										if(account.id != cheque.toAccount) {
											out.println("<FAILED>");
											System.out.println("wrong recipient");
											break;
										}
										
										Coin coin = bank.vault.cashinCheque(cheque);
										if(coin == null) {
											out.println("<FAILED>");
											System.out.println("invalid cheque");
											break;
										}
										System.out.println("Cheque is valid");
										out.println("<SUCCESS>");
										out.println(coin.serialize());
									} catch(Exception e) {
										System.out.println("Decryption of cheque failed");
										e.printStackTrace();
										out.println("<FAILED>:Decryption failed");
									}
									break;
								default:
									Error();
									break outerloop;
							}
						}
					}
					break;
				default:
					Error();
					break outerloop;
				}	
			}
		} catch (IOException e) {
			System.out.println("client didn't close socket " + e);
			e.printStackTrace();
		} finally {
			try {
				client.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Session ended: " + Thread.currentThread().getName());
	}
	
	private void Error() {
		System.out.println("invalid message received, connection closing...");
	}
}
