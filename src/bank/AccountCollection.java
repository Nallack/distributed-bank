package bank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import common.EncryptionHelper;

public class AccountCollection {
	private List<Account> accounts;
	File file;
	
	//private List<Coin> coins;
	
	public AccountCollection(String filename) {
		this.accounts = new ArrayList<Account>();
		//create file
		this.file = new File(filename);
		if (!file.exists()) {
			try {
				new FileWriter(file).close();
			}
			catch (Exception e) {
				System.out.println("couldnt create file " + filename);
			}
		}
		else if (!file.isDirectory()) {
			loadFromFile();
		}
	}
	
	public boolean accountExists(String username) {
		for(Account account : accounts) {
			if(account.username.equals(username)) {
				return true;
			}
		}
		return false;
	}
	
	//return the newly created account after adding it to the collection
	public Account createAccount(String username, char[] password) {
		if(accountExists(username)) return null;
		
		long id = accounts.size();
		KeyPair keyPair;
		keyPair = EncryptionHelper.rsaGenerateKey(id);
		Account account = new Account(id, username, password, keyPair);
		accounts.add(account);
		saveToFile();
		return account;
	}
	
	//return the account id only if both fields match
	public Account getAccountByLogin(String username, char[] password) {
		for(Account account : accounts) {
			if(account.username.equals(username)) {
				if(Arrays.equals(password, account.password))
					return account;
				else {
					return null;
				}
			}
		}
		return null;
	}
	
	public Account getAccountById(long id) {
		for(Account account : accounts) {
			if(account.id == id) {
				return account;
			}
		}
		return null;
	}
	
	private void loadFromFile() {
		BufferedReader br;
		try {
			br = new BufferedReader(
				 new FileReader(file));
			try {
				String line = null;
				while ((line = br.readLine()) != null) {
					accounts.add(Account.deserialize(line));
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			} finally {
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void saveToFile() {
		try {
			BufferedWriter bw = new BufferedWriter(
								new FileWriter(file));
			
			for (Account a : accounts) {				
				bw.write(a.serialize() + '\n');
			}
			bw.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String toString() {
		if(accounts.isEmpty()) return "Empty";
		StringBuilder sb = new StringBuilder();
		for(Account account : accounts) {
			sb.append(account.toString() + '\n');
		}
		return sb.toString();
	}

}
