package analyser;

public enum AnalyserType {
	None,
	Average,
	Median
}
