package analyser;

import java.io.IOException;

import javax.net.ssl.SSLSocket;

import common.ClientSession;
import director.Director;

public class AnalyserClientSession extends ClientSession {

	private Director director;
	
	public AnalyserClientSession(Director director, SSLSocket server) throws IOException {
		super(server);
		this.director = director;
		if(!in.readLine().equals("<ANALYSER>")) {
			disconnect();
			throw new IOException("Not an analyser");
		}
	}
	
	public boolean sendCheque(long fromId, String cheque) throws IOException {
		out.println("<SEND>");
		out.println(fromId);
		out.println(cheque);
		if(in.readLine().equals("<SUCCESS>")) return true;
		else return false;
	}
	
	public String exchangeData(String data) throws IOException {
		out.println(data);
		if(!in.readLine().equals("<SUCCESS>")) return null;
		return in.readLine();
	}

}