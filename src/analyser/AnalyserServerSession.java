package analyser;

import java.io.IOException;

import javax.net.ssl.SSLSocket;

import bank.BankClientSession;
import common.Coin;
import common.ServerSession;

public class AnalyserServerSession extends ServerSession implements Runnable {

	private Analyser analyser;
	
	public AnalyserServerSession(Analyser analyser, SSLSocket client) {
		super(client);
		this.analyser = analyser;
	}
	
	public void run() {
		System.out.println("Session started: " + Thread.currentThread().getName());		
		try {
			out.println("<ANALYSER>");
			String message = null;
			
			outerloop:
			while((message = in.readLine()) != null) {
				switch(message) {
					case "<SEND>": {
						long fromAccountId = Long.parseLong(in.readLine());
						String cheque = in.readLine();
						
						System.out.println("Encrypted Cheque Received: " + cheque);
						
						BankClientSession bankSession = analyser.createBankClientSession();
						
						bankSession.login(analyser.username, analyser.password);
						Coin coin = bankSession.cashin(fromAccountId, cheque);
						if(coin == null) {
							System.out.println("Cheque was invalid");
							out.println("<FAILED>");
						}
						else {
							analyser.wallet.add(coin);
							System.out.println("Cheque is valid");
							out.println("<SUCCESS>");
						}
						
						String data = in.readLine();
						
						System.out.println("processing data " + data);
						String result = AnalyserCalc.analyse(analyser.serviceType, data);
						if(result == null) {
							System.out.println("processing failed");
							out.println("<FAILED>");
							break;
						}
						System.out.println(analyser.serviceName + " is " + result);
						out.println("<SUCCESS>");
						out.println(result);
						
						bankSession.disconnect();
						
						break;
					}
					default: {
						System.out.println("invalid message received");
						client.close();
						break outerloop;
					}
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				client.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Session ended: " + Thread.currentThread().getName());
	}
}
