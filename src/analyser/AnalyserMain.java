package analyser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Integer;

import bank.BankClientSession;

public class AnalyserMain {
	
	//addresses and ports
	static int directorPort = 4444;
	static int bankPort = 1234;
	
	public static void main(String[] args) {
		
		String security = "SSL";
		String keyStore = "analyser.jks";
		String trustStore = "analyser.ts";
		char[] ksPassword = "analyserpassword".toCharArray();
		char[] tsPassword = "analyserpassword".toCharArray();
		
		Analyser analyser = new Analyser();
		analyser.loadFactory(security, keyStore, ksPassword, trustStore, tsPassword);
		System.out.println("Please enter 'demo' to start");
		commandLoop(analyser);
	}
	
	//register with director first. if successful, run an analyser server.
	private static void setupAnalyser(Analyser analyser) {
		
		//Connect to bank to get accountId
		long accountId = -1L;
		try { 
			BankClientSession bankSession = analyser.createBankClientSession(); //this
			
			try {
				System.out.println("Creating account...");
				if(!bankSession.createAccount(analyser.username, analyser.password)) {
					System.out.println("Account already exists");
				}
				
				if(!bankSession.login(analyser.username, analyser.password)) {
					System.out.println("Failed to login");
					bankSession.disconnect();
					return;
				}
				
				accountId = bankSession.getId();
			} catch (IOException e) { 
				System.out.println("Error: " + e);
				e.printStackTrace();
				return;
			}
			
			bankSession.disconnect();
		} catch (IOException e) { 
			System.out.println("Error: couldn't connect to bank"); 
			return;
		}
		
		
		System.out.println("Account ID = " + accountId);
		
		try {
			if(analyser.registerWithDirector(
					analyser.serviceName, 
					analyser.serviceType,
					analyser.serverPort,
					accountId)) {
				System.out.println("Registration successful with director as '" + analyser.serviceName + "'");
			}
			else {
				System.out.println("Registration with " + analyser.directorAddress + " failed");
			}
		} catch(IOException e) {
			System.out.println("Registration with " + analyser.directorAddress + " failed");
		}
		
		try {
			analyser.startServer(analyser.serverPort);
		} catch(Exception e) {
			System.out.println("Failed to start server.");
		}
	}
	
	private static void commandLoop(Analyser analyser) {
		BufferedReader input = new BufferedReader(
	   			   new InputStreamReader(
	   				   System.in));
		
		try {
			String command = null;
			while(true) {
				command = input.readLine();
				switch(command) {
				case "demoAverage1":
					analyser.username = "analyser1";
					System.out.println("Starting demo\nPlease enter bank IP Address");
					analyser.bankAddress = input.readLine();
					analyser.bankPort = bankPort;
					System.out.println("Please enter director IP Address");
					analyser.directorAddress = input.readLine();
					analyser.directorPort = directorPort;
					System.out.println("Please enter a server port number between 1024-49151");
					while(true) {
						try {
							int tempPort = Integer.parseInt(input.readLine());
							if ((tempPort >= 1024) && (tempPort <= 49151)) {
								analyser.serverPort = tempPort;
								break;
							}
							else {
								System.out.println("That is not a valid port number");
							}
						}
						catch (Exception e) {
							System.out.println("That is not a valid port number");	
						}
					}
					setupAnalyser(analyser);
					break;
				case "demoAverage2":
					analyser.username = "analyser12";
					System.out.println("Starting demo\nPlease enter bank IP Address");
					analyser.bankAddress = input.readLine();
					analyser.bankPort = bankPort;
					System.out.println("Please enter director IP Address");
					analyser.directorAddress = input.readLine();
					analyser.directorPort = directorPort;
					System.out.println("Please enter a server port number between 1024-49151");
					while(true) {
						try {
							int tempPort = Integer.parseInt(input.readLine());
							if ((tempPort >= 1024) && (tempPort <= 49151)) {
								analyser.serverPort = tempPort;
								break;
							}
							else {
								System.out.println("That is not a valid port number");
							}
						}
						catch (Exception e) {
							System.out.println("That is not a valid port number");	
						}
					}
					setupAnalyser(analyser);
					break;
				case "demoMode":
					analyser.serviceName = "mode";
					analyser.username = "analyser1";
					System.out.println("Starting demo\nPlease enter bank IP Address");
					analyser.bankAddress = input.readLine();
					analyser.bankPort = bankPort;
					System.out.println("Please enter director IP Address");
					analyser.directorAddress = input.readLine();
					analyser.directorPort = directorPort;
					System.out.println("Please enter a server port number between 1024-49151");
					while(true) {
						try {
							int tempPort = Integer.parseInt(input.readLine());
							if ((tempPort >= 1024) && (tempPort <= 49151)) {
								analyser.serverPort = tempPort;
								break;
							}
							else {
								System.out.println("That is not a valid port number");
							}
						}
						catch (Exception e) {
							System.out.println("That is not a valid port number");	
						}
					}
					setupAnalyser(analyser);
					break;
				case "demoMedian":
					analyser.serviceName = "median";
					analyser.username = "analyser2";
					System.out.println("Starting demo\nPlease enter bank IP Address");
					analyser.bankAddress = input.readLine();
					analyser.bankPort = bankPort;
					System.out.println("Please enter director IP Address");
					analyser.directorAddress = input.readLine();
					analyser.directorPort = directorPort;
					System.out.println("Please enter a server port number between 1024-49151");
					while(true) {
						try {
							int tempPort = Integer.parseInt(input.readLine());
							if ((tempPort >= 1024) && (tempPort <= 49151)) {
								analyser.serverPort = tempPort;
								break;
							}
							else {
								System.out.println("That is not a valid port number");
							}
						}
						catch (Exception e) {
							System.out.println("That is not a valid port number");	
						}
					}
					setupAnalyser(analyser);
					break;
				case "demoRange":
					analyser.serviceName = "range";
					analyser.username = "analyser3";
					System.out.println("Starting demo\nPlease enter bank IP Address");
					analyser.bankAddress = input.readLine();
					analyser.bankPort = bankPort;
					System.out.println("Please enter director IP Address");
					analyser.directorAddress = input.readLine();
					analyser.directorPort = directorPort;
					System.out.println("Please enter a server port number between 1024-49151");
					while(true) {
						try {
							int tempPort = Integer.parseInt(input.readLine());
							if ((tempPort >= 1024) && (tempPort <= 49151)) {
								analyser.serverPort = tempPort;
								break;
							}
							else {
								System.out.println("That is not a valid port number");
							}
						}
						catch (Exception e) {
							System.out.println("That is not a valid port number");	
						}
					}
					setupAnalyser(analyser);
					break;
				default:
					printHelp();
					break;
				}
				
			}
		} catch (IOException e) {
			System.out.println(e);
		}
	}
	
	private static void printHelp() {
		System.out.println(	  "commands:\n"
							+ "demoAverage1, demoAverage2, demoMedian, demoMode, demoRange");
	}
}
