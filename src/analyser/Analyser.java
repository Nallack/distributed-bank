package analyser;

import java.io.IOException;

import javax.net.ssl.*;

import bank.BankClientSession;
import common.SocketHelper;
import common.Wallet;
import director.DirectorClientSession;

public class Analyser {
	
	/*Change these variables when setting up new analysers
	 username *must* be different    */
	
	public String serviceName = "average1";
	public String serviceType = "average";
	public String username = "analyser";
	public char[] password = "password".toCharArray();
	public String walletFilename = "walletAnalyser.csv";
	
	public String bankAddress = null;
	public int bankPort = -1;
	
	public String directorAddress = null;
	public int directorPort = -1;
	
	AnalyserType type = AnalyserType.None;
	SSLServerSocketFactory serverFactory;
	SSLSocketFactory clientFactory;
	Thread serverThread;
	
	Wallet wallet = null;
	
	public int serverPort = 6666;
	
	public Analyser() {		
		wallet = new Wallet(walletFilename);
	}
	
	public void loadFactory(String security, String keyStore, char[] ksPassword, String trustStore, char[] tsPassword) {
		try {
			serverFactory = SocketHelper.createSSLServerSocketFactory(security, keyStore, ksPassword);
			clientFactory = SocketHelper.createSSLSocketFactory(security, trustStore, tsPassword);
		} 
		catch (Exception e) { 
			e.printStackTrace();
		}
	}

	public BankClientSession createBankClientSession() throws IOException {
		
		SSLSocket server = (SSLSocket)clientFactory.createSocket(bankAddress, bankPort);
		return new BankClientSession(server);
	}
	
	public void startServer(int serverPort) {
		this.serverPort = serverPort;
		serverThread = new Thread(dispatcher);
		serverThread.start();
	}
	
	public void stopServer() {
		serverThread.interrupt();
	}
	
	public boolean registerWithDirector(String name, String type, int port, long accountId) throws IOException {
		SSLSocket director = (SSLSocket)clientFactory.createSocket(directorAddress, directorPort);		
		DirectorClientSession session = new DirectorClientSession(director); 
		//determines if this analyser already exists on director routing table
		if (session.register(name, type, port, accountId)) {
			director.close();
			return true;
		}
		else {
			director.close();
			return false;
		}
	}
	
	Runnable dispatcher = new Runnable() {
		public void run() {
			try {
				SSLServerSocket serverSocket = (SSLServerSocket)serverFactory.createServerSocket(serverPort);
				try {
					System.out.println("Server started");
					while(true) {
						System.out.println("Waiting for client...");
						SSLSocket socket = (SSLSocket)serverSocket.accept();
						System.out.println("Client accepted");
						new Thread(new AnalyserServerSession(Analyser.this, socket)).start();
					}
				} catch(IOException e) {
					System.out.println(e);
				} finally {
					serverSocket.close();
				}
				
			} catch(IOException e) {
				System.out.println(e);
			}
		}
	};
}
