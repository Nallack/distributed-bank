package analyser;

import java.util.HashMap;
import java.util.Arrays;

public class AnalyserCalc {
	
	//this stays the same for all analysers
	public static String analyse(String serviceName, String data) {
		switch (serviceName) {
			case "range": {
				return range(data);
			}
			case "average": {
				return average(data);
			}	
			case "median": {
				return mode(data);
			}	
			case "mode": {
				return mode(data);
			}	
		}
		return range(data);
	}

	//different types of analysis:
	private static String average(String data) {
		String[] split = data.split(",");
		int total = 0;
		double sum = 0;
		for(String s : split) {
			sum += Double.parseDouble(s);
			total++;
		}
		return Double.toString(sum/total);
	}
	
	private static String median(String data) {
		String[] split = data.split(",");
		Double[] a = new Double[split.length];
		for (int i = 0; i < split.length; i++) {
			a[i] = Double.parseDouble(split[i]);
		}
		Arrays.sort(a);
		int middle = a.length/2;
		return Double.toString(a[middle]);
	}
	
	private static String range(String data) {
		String[] split = data.split(",");
		double low = 0.00;
		double high = 0.00;
		double current = 0.00;
		for(int i = 0; i < split.length; i++) {
			current = Double.parseDouble(split[i]);
			if (i == 0) {
				low = current; high = current;
			}
			if (current < low) low = current;
			else if (current > high) high = current;
		}
		return Double.toString(high - low);
	}

	private static String mode(String data) {
		String[] split = data.split(",");
		HashMap<Double, Integer> frequency = new HashMap<Double, Integer>();
		int current = 0;
		double temp;
		for(int i = 0; i < split.length; i++) {
			temp = Double.parseDouble(split[i]);
			if (frequency.containsKey(temp)) {
				frequency.put(temp, frequency.get(temp)+1);
			}
			else {
				frequency.put(Double.parseDouble(split[i]), 1);
			}
			if (frequency.get(temp) > current) current = frequency.get(temp);
		}
		return Double.toString(current);
	}
}
