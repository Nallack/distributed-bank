package director;

import java.io.IOException;

import javax.net.ssl.*;

import common.SocketHelper;

public class Director {

	SSLServerSocketFactory serverFactory;
	SSLSocketFactory clientFactory;
	SSLServerSocket serverSocket;
	RoutingTable analysers;
	
	//defualts
	String security = "SSL";
	String keyStore = "keystore.jks";
	char[] ksPassword = "bankpassword".toCharArray();
	String trustStore = "truststore.ts";
	char[] tsPassword = "clientpassword".toCharArray();
	
	int serverPort = 4444;
	
	public Director() {
		analysers = new RoutingTable();
	}
	
	public void loadFactory(String security, String keyStore, char[] ksPassword, String trustStore, char[] tsPassword) {
		try {
			serverFactory = SocketHelper.createSSLServerSocketFactory(security, keyStore, ksPassword);
			clientFactory = SocketHelper.createSSLSocketFactory(security, trustStore, tsPassword);
		} catch (Exception e) { 
			e.printStackTrace();
		}
	}
	
	public void startServer(int port) {
		this.serverPort = port;
		new Thread(dispatcher).start();
	}
	
	public Runnable dispatcher = new Runnable () {
		public void run() {
			System.out.println("Running Director Server");
			try {
				serverSocket = (SSLServerSocket)serverFactory.createServerSocket(serverPort);
				try {
					while(true) {
						System.out.println("Waiting for client...");
						SSLSocket socket = (SSLSocket)serverSocket.accept();
						new Thread(new DirectorServerSession(Director.this, socket)).start();
					}
				} catch(IOException e) {
					throw e;
				} finally {
					serverSocket.close();
				}
							
			} catch (IOException e) {
					e.printStackTrace();
			}
		}
	};
}
