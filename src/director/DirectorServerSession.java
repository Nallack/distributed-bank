package director;

import java.io.IOException;

import javax.net.ssl.SSLSocket;

import analyser.AnalyserClientSession;
import common.ServerSession;

public class DirectorServerSession extends ServerSession implements Runnable {

	private Director director;
	
	public DirectorServerSession(Director director, SSLSocket client) {
		super(client);
		this.director = director;
	}
	
	public void run() {
		System.out.println("Session started: " + Thread.currentThread().getName());
		
		try {
			out.println("<DIRECTOR>");
			
			String message = null;
			outerloop:
			while((message = in.readLine()) != null) {
				System.out.println(message);
				
				switch(message) {
				case "<REGISTER>": {
					
					//save the socket ip to a name in the routing table
					String name = in.readLine();
					String type = in.readLine();
					int port = Integer.parseInt(in.readLine());
					long accountId = Long.parseLong(in.readLine());
					if(director.analysers.subscribe(name, type, client.getInetAddress(), port, accountId)) {
						out.println("<SUCCESS>");
						System.out.println("analyser '" + name + "' was successfully registered");
					} else {
						out.println("<FAIL>");
						System.out.println("analyser '" + name + "' failed registration");
					}
					break;
				}
				case "<GET>": {
					//return the names in the routing table
					out.println("<START>");
					out.println(director.analysers.getNames());
					out.println("<END>");
					break;
				}
				case "<SUBSCRIBE>": {
					try {
						String analyserType = in.readLine();
						AnalyserRegister register = director.analysers.get(analyserType);
						if (register != null) {
							out.println("<SUCCESS>");
							out.println(register.accountId);
						}
						else {
							out.println("<FAILED>");
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}
					break;
				}
				case "<SEND>": {
					//forward cheque then data to the analyser
					
					//create session with analyser
					String analyserName = in.readLine();
					long fromAccountId = Long.parseLong(in.readLine());
					String cheque = in.readLine();
					
					
					AnalyserRegister register = director.analysers.get(analyserName);
					if(register == null) {
						out.println("<FAILED>");
						break;
					}
					System.out.println("connecting to analyser at " + register.address.getHostAddress() + ":" + register.port);
					SSLSocket analyser = (SSLSocket)director.clientFactory.createSocket(register.address.getHostAddress(), register.port);
					AnalyserClientSession analyserSession = new AnalyserClientSession(director, analyser);
						
					if(!analyserSession.sendCheque(fromAccountId, cheque)) {
						out.println("<FAILED>");
						analyserSession.disconnect();
						break;
					}
					out.println("<SUCCESS>");
					String result = analyserSession.exchangeData(in.readLine());
					if(result == null) {
						out.println("<FAILED>");
						analyserSession.disconnect();
					}
					out.println("<SUCCESS>");
					out.println(result);
					
					System.out.println("transfer was successful");
					
					break;
				}
				default:
					Error();
					break outerloop;
				}
			}
		} catch(IOException e) {
			//e.printStackTrace();
			System.out.println("input was null");
		} finally {
			try {
				client.close();
			}
			catch (Exception e) {
				//e.printStackTrace();
			}
		}
		System.out.println("Session ended: " + Thread.currentThread().getName());
	}
	
	private void Error() {
		System.out.println("invalid message received, connection closing...");
	}
}
