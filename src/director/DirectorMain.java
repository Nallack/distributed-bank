package director;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class DirectorMain {
	public static void main(String[] args) {
		
		String security = "SSL";
		String keyStore = "director.jks";
		char[] ksPassword = "directorpassword".toCharArray();
		String trustStore = "director.ts";
		char[] tsPassword = "directorpassword".toCharArray();
		
		Director director = new Director();
		director.loadFactory(security, keyStore, ksPassword, trustStore, tsPassword);
		director.startServer(4444);
		
		System.out.println("Directory Ready");
		
		commandLoop(director);
		
	}
	
	private static void commandLoop(Director director) {
		BufferedReader input = new BufferedReader(
				   			   new InputStreamReader(
				   				   System.in));

		try {
			String command = null;
			while(true) {
				command = input.readLine();
				System.out.println("Command: " + command);
				switch(command) {
				case "analysers":
					System.out.println(director.analysers);
					break;
				default:
					printHelp();
					break;
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void printHelp() {
		System.out.println(	  "commands:\n"
							+ "analysers");
	}
}
