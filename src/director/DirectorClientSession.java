package director;

import java.io.IOException;
import java.security.PrivateKey;

import javax.net.ssl.SSLSocket;

import common.Cheque;
import common.ClientSession;
import common.EncryptionHelper;

public class DirectorClientSession extends ClientSession {

	public DirectorClientSession(SSLSocket server) throws IOException {
		super(server);
		if(!in.readLine().equals("<DIRECTOR>")) {
			disconnect();
			throw new IOException("Not a director");
		}
	}

	//verify director can connect to the named analyser and retreive it's account number
	public long subscribe(String analyserName) throws IOException {
		out.println("<SUBSCRIBE>");
		out.println(analyserName);
		try {
			if (in.readLine().equals("<SUCCESS>")) {
				return Long.parseLong(in.readLine());
			}
			else {
				return -1;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public String sendData(String analyserName, Cheque cheque, PrivateKey key, String data) throws Exception {
		try {
			out.println("<SEND>");
			out.println(analyserName);
			out.println(cheque.fromAccount);
			out.println(EncryptionHelper.rsaEncrypt(cheque.serialize(), key));
			
			//wait for success if cheque was successfully deposited
			String message;
			message = in.readLine();
			if (!message.equals("<SUCCESS>")) {
				System.out.println(message);
				return null;
			}
			out.println(data);
			
			//wait to confirm data was succesfully processed
			message = in.readLine();
			if (!message.equals("<SUCCESS>")) {
				System.out.println(message);
				return null;
			}
			return in.readLine();
		}
		catch (Exception e) {
			disconnect();
			throw e;
		}
	}
	
	public String[] getAnalysers() {
		try {
			out.println("<GET>");
			return in.readLine().split(",");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param name
	 * @param port
	 * @param accountId
	 * @return true if registration was successful
	 * @throws IOException
	 */
	public boolean register(String name, String type, int port, long accountId) throws IOException {
		out.println("<REGISTER>");
		out.println(name);
		out.println(type);
		out.println(port);
		out.println(accountId);
		if(in.readLine().equals("<SUCCESS>")) return true;
		else return false;
	}
}
