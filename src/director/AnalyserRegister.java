package director;

import java.net.InetAddress;

public class AnalyserRegister {
	String type;
	InetAddress address;
	int port;
	long accountId;
	
	public AnalyserRegister(String type, InetAddress address, int port, long accountId) {
		this.type = type;
		this.address = address;
		this.port = port;
		this.accountId = accountId;
	}
}
