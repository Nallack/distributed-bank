package director;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

public class RoutingTable {
	Map<String, AnalyserRegister> table;
	
	public RoutingTable() {
		this.table = new HashMap<String, AnalyserRegister>();
		
	}
	
	public boolean subscribe(String name, String type, InetAddress address, int port, long accountId) {
		if(!table.containsKey(name)) {
			//System.out.println("Sub: " + name + " : " + address + " : " + port + " : " + accountId); 
			table.put(name, new AnalyserRegister(type, address, port, accountId));
			return true;
		} else {
			return false;
		}
	}
	
	public String getNames() {
		StringBuilder sb = new StringBuilder();
		for(String name : table.keySet()) {
			sb.append(name + '\n');
		}
		return sb.toString();
	}
	
	public AnalyserRegister get(String type) {
		for(AnalyserRegister register : table.values()) {
			if(register.type.equals(type)) return register;
		}
		return null;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(String name : table.keySet()) {
			sb.append(String.format("name: %s\t address: %s\t port: %s\t accountId: %s\n", 
									name, table.get(name).address, table.get(name).port, table.get(name).accountId));
		}
		return sb.toString();
	}
	

}
