package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JTextField;

public class TextFieldInputStream extends InputStream {
	
	private JTextField textField;
	private final StringBuilder sb;
	
	private ActionListener inputAction = new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			System.out.println(textField.getText());
			sb.append(textField.getText() + '\r' + '\n');
			textField.setText(null);
		}
	};
	
	public TextFieldInputStream(final JTextField textField) {
		this.textField = textField;
		sb = new StringBuilder();
		textField.addActionListener(inputAction);
	}

	//Buffered Reader uses this, needs to be blocking
	@Override
	public int read(byte[] b, int off, int len) {
		
		//block until input is available
		//should probably try to use notify on actionperformed
		while(true) {
			try {
				if(sb.length() == 0) {
					Thread.sleep(100);
				}
				else break;
			}
			catch(Exception e) {
				//e.printStackTrace();
				return -1;
			}
		}		
		int i = 0;
		while(sb.length() != 0 && i < len) {
			b[i] = (byte)sb.charAt(0);
			sb.deleteCharAt(0);
			i++;
		}
		return i;
	}
	
	@Override
	public void close() {
		textField.removeActionListener(inputAction);
	}
	
	@Override
	public int available() {
		return sb.length();
	}
	
	@Override
	public int read() throws IOException {
		
		if(sb.length() == 0) return -1;
		
		while(true) {
			try {
				if(sb.length() == 0) {
					Thread.sleep(100);
				} else break;
			} catch(InterruptedException e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		char c = sb.charAt(0);
		System.out.println("returning " + c);
		sb.deleteCharAt(0);
		return c;
	}	
}
